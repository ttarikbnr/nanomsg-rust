use tokio;
use anyhow::Result;
use nanomsg_rust::NanomsgReply;

#[tokio::main]
async fn main() -> Result<()> {
    let mut socket = NanomsgReply::new(5656, request_handler);
    socket.bind().await?;
    socket.serve().await?;
    Ok(())
}

async fn request_handler(_request: Vec<u8>) -> Vec<u8> {
    b"true".to_vec()
}
