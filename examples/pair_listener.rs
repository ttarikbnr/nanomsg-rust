use tokio;
use anyhow::Result;
use nanomsg_rust::NanomsgPairListener;

#[tokio::main]
async fn main() -> Result<()> {
    let mut listener = NanomsgPairListener::new();
    listener.bind(8001).await?;

    loop {
        let mut socket = listener.accept().await?;
        let f = async move {
            loop {
                if let Ok(msg) = socket.read().await {
                    if let Err(err) = socket.write(msg).await {
                        eprintln!("{}", err);
                    }
                } else {
                    break;
                }
            }
        };
        tokio::spawn(f);
    }
}
