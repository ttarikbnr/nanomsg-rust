use tokio;
use anyhow::Result;
use nanomsg_rust::NanomsgPair;

#[tokio::main]
async fn main() -> Result<()> {
    let mut socket = NanomsgPair::new();
    socket.connect("127.0.0.1:8001".to_string()).await?;

    loop {
        socket.write(b"Hello World".to_vec()).await.unwrap();
        let msg = socket.read().await.unwrap();
        println!("Incoming msg : {}", String::from_utf8_lossy(msg.as_ref()));
    }
}
