use tokio;
use anyhow::Result;
use nanomsg_rust::NanomsgBus;

#[tokio::main]
async fn main() -> Result<()> {
    let mut socket = NanomsgBus::new("127.0.0.1:7777".to_string());

    loop {
        let msg = socket.read().await?;
        println!("Message is {}", String::from_utf8_lossy(&*msg));
    }
}
