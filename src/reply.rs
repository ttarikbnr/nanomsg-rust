use tokio::net::{TcpListener, TcpStream};
use tokio::prelude::*;
use tokio::time::timeout;
use std::time::Duration;
use std::io::ErrorKind;
use log::{error, warn, info}; 
use futures::Future;

const HANDSHAKE_WRITE_TIMEOUT_MS : u64 = 250;
const REP_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x31, 0x00, 0x00];
const REQ_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x30, 0x00, 0x00];


pub struct NanomsgReply<S, F> {
    binding_port : u16,
    listener     : Option<TcpListener>,
    service_fn   : S,
    _f           : std::marker::PhantomData<F> 
}

impl <S, F>NanomsgReply<S, F>
    where S: FnMut(Vec<u8>) -> F + Clone + Send + 'static,
          F: Future<Output=Vec<u8>> + Send + 'static{
    pub fn new(binding_port: u16,
               service_fn: S) -> Self {
        Self {
            binding_port,
            service_fn,
            listener        : None,
            _f              : std::marker::PhantomData
        }
    }

    pub async fn bind(&mut self) -> std::io::Result<()> {
        let listener = TcpListener::bind(("0.0.0.0", self.binding_port)).await?;

        self.listener = Some(listener);
        Ok(())
    }

    async fn accept(&mut self) -> std::io::Result<NanomsgReplySocket<S, F>> {
        if let Some(listener) = self.listener.as_mut() {
            let (mut tcpstream, socket_addr) = listener.accept().await?;
            info!("New connection came. Address {}.", socket_addr);
            set_tcp_options(&mut tcpstream)?;
            tcpstream.write_all(&REP_HANDSHAKE_PACKET[..]).await?;

            let duration = Duration::from_millis(HANDSHAKE_WRITE_TIMEOUT_MS);

            let mut handshake = [0u8; 8];

            match timeout(duration, tcpstream.read_exact(handshake.as_mut())).await {
                Ok(_res) => {
                    // Check handshake packet
                    if handshake == REQ_HANDSHAKE_PACKET {
                        info!("Handshake is successfull.");
                        let nanomsg_rep_socket = NanomsgReplySocket::new(tcpstream, self.service_fn.clone());
                        Ok(nanomsg_rep_socket)
                    } else {
                        warn!("Handshake is unsuccessfull.");
                        Err(wrong_protocol_err())
                    }
                },
                Err(_err) => {
                    Err(timeout_err("Handshake reading timedout."))
                }
            }
        } else {
            error!("No socket listener.");
            panic!("No socket listener.")
        }
    }

    pub async fn serve(mut self) -> std::io::Result<()> {
        loop {
            match self.accept().await {
                Ok(reply_socket) => {
                    tokio::spawn(reply_socket.reply());
                }
                Err(err) => {
                    error!("Got error while accepting socket. {}", err);
                }
            }
        }
    }
    
}

fn set_tcp_options(_tcpstream: &mut TcpStream) -> io::Result<()>{
    // tcpstream.set_nodelay(true)?;
    Ok(())
}

fn timeout_err(error_msg: &'static str) -> std::io::Error {
    std::io::Error::new(ErrorKind::TimedOut, error_msg)
}

fn wrong_protocol_err() -> std::io::Error {
    std::io::Error::new(ErrorKind::Other, "Wrong protocol.")
}


struct NanomsgReplySocket<S, F> {
    socket      : TcpStream,
    service_fn  : S,
    _f          : std::marker::PhantomData<F> 
}

impl <S, F> NanomsgReplySocket <S, F>
    where S: FnMut(Vec<u8>) -> F,
          F: Future<Output=Vec<u8>> + Send + 'static{

    fn new(socket     : TcpStream,
           service_fn : S) -> Self {
        Self {
            socket,
            service_fn,
            _f          : std::marker::PhantomData
        }
    }

    async fn write_all(&mut self, request_id: u32, payload: &[u8]) -> std::io::Result<()> {
        let request_id_length = 4;

        let payload_size = payload.len() as u64 + request_id_length;

        self.socket.write_u64(payload_size).await?;

        // Set request id's most significant bit.
        self.socket.write_u32(request_id | 0x80000000).await?;

        self.socket.write_all(payload).await
    }

    async fn read(&mut self) -> std::io::Result<(u32, Vec<u8>)> {
        let payload_size = self.socket.read_u64().await?; 

        // TODO check if this is channel id or request id
        let request_id = self.socket.read_u32().await? & 0x7FFFFFFF;

        let mut payload = vec![0; payload_size as usize - 4];

        self.socket.read_exact(payload.as_mut()).await?;

        Ok((request_id, payload))
    }

    async fn reply(mut self) -> std::io::Result<()> {
        loop {
            // Read request
            let (request_id, payload) = self.read().await?;

            // Run service function
            let reply = (self.service_fn)(payload).await;

            // Write reply
            self.write_all(request_id, reply.as_ref()).await?;
        }
    }
}
