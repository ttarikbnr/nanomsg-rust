use crate::options::SocketOptions;
use tokio::net::TcpStream;
use tokio::net::tcp::{WriteHalf, ReadHalf};
use tokio::sync::{oneshot, mpsc, Mutex};
use tokio::prelude::*;
use std::io;
use std::io::ErrorKind;
use std::task::Poll;
use std::future::Future;
use std::sync::Arc;
use log::{info, error};
use ttl_cache::TtlCache;
use futures::future;

const REP_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x31, 0x00, 0x00];
const REQ_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x30, 0x00, 0x00];

use std::sync::atomic::{AtomicUsize, Ordering};

const STATE_STARTING        : usize = 0;
const STATE_RECONNECTING    : usize = 1;
const STATE_CONNECTED       : usize = 2;
const STATE_ERROR           : usize = 3;

#[derive(Clone)]
struct BackgroundTaskStateAtomic{
    state : Arc<AtomicUsize>, 
}

impl BackgroundTaskStateAtomic {
    fn new() -> Self {
        Self {
            state: Arc::new(AtomicUsize::new(STATE_STARTING))
        }
    }

    fn set_connected(&self) {
        log::info!("Connected");
        self.state.store(STATE_CONNECTED, Ordering::Release);
    }

    fn set_reconnecting(&self) {
        log::info!("Reconnecting");
        self.state.store(STATE_RECONNECTING, Ordering::Release);
    }

    fn set_error(&self) { 
        self.state.store(STATE_ERROR, Ordering::Release);
    }
    
    fn get_state(&self) -> BackgroundTaskState {
        let state = self.state.load(Ordering::Acquire);
        match state {
            STATE_STARTING      => BackgroundTaskState::Starting,
            STATE_CONNECTED     => BackgroundTaskState::Connected,
            STATE_RECONNECTING  => BackgroundTaskState::Reconnecting,
            STATE_ERROR         => BackgroundTaskState::Error,
            _                   => unreachable!(),
        }
    }
}

use BackgroundTaskState::*;
#[derive(Debug, Clone, Copy)]
enum BackgroundTaskState {
    Starting,
    Connected,
    Reconnecting,
    Error
}

pub struct NanomsgRequest {
    address                     : String,
    request_chan                : mpsc::UnboundedSender<Request>,
    background_task_cancel_tx   : Option<oneshot::Sender<()>>,
    socket_options              : SocketOptions,
    bg_task_state               : BackgroundTaskStateAtomic,
}

impl NanomsgRequest {
    pub fn new(address: String) -> Self {

        let socket_options = SocketOptions::default();
        Self::new_with_socket_options(address, socket_options)
    }

    pub fn new_with_socket_options(address          : String,
                                   socket_options   : SocketOptions) -> Self {
        let (request_tx, request_rx) = mpsc::unbounded_channel();
        let (bg_closer_tx, bg_closer_rx) = oneshot::channel();
        let bg_task_state = BackgroundTaskStateAtomic::new();

        spawn_background_task(address.clone(),
                              request_rx, 
                              bg_closer_rx, 
                              socket_options.clone(),
                              bg_task_state.clone());

        Self {
            address,
            socket_options,
            bg_task_state,
            request_chan              : request_tx,
            background_task_cancel_tx : Some(bg_closer_tx),
        }
    }

    #[allow(unused)]
    fn respawn_background_task(&mut self) {
        info!("Respawning background task.");

        let (request_tx, request_rx) = mpsc::unbounded_channel();
        let (bg_closer_tx, bg_closer_rx) = oneshot::channel();
        let bg_task_state = BackgroundTaskStateAtomic::new();
        
        spawn_background_task(self.address.clone(),
                              request_rx,
                              bg_closer_rx, 
                              self.socket_options.clone(),
                              bg_task_state.clone());

        self.request_chan = request_tx;
        self.bg_task_state = bg_task_state;
        self.background_task_cancel_tx = Some(bg_closer_tx);
    }

    pub fn request(&self, payload: Vec<u8>) -> impl Future<Output=io::Result<Vec<u8>>> + Send + 'static {
        let (tx, rx) = oneshot::channel();
        let request = Request { payload, reply_sender: tx};
        let req_tx = self.request_chan.clone();
        let bg_task_state = self.bg_task_state.get_state();

        match bg_task_state {
            Starting | Reconnecting => {
                let wait = self.socket_options.get_reconnect_try_interval();
                if let Err(err) = req_tx.send(request) {
                    let err_msg = format!("Couldn't send request to background task. {}", err);
                    future::Either::Left(future::err(other_err(err_msg)))
                } else {
                    future::Either::Right(reply_fut(rx, true, wait))
                }
            } 
            Connected => {
                if let Err(err) = req_tx.send(request) {
                    let err_msg = format!("Couldn't send request to background task. {}", err);
                    future::Either::Left(future::err(other_err(err_msg)))
                } else {
                    future::Either::Right(reply_fut(rx, false, None))
                }
            }
            Error => {
                let err_msg = "Background task is not running.".to_string();
                future::Either::Left(future::err(other_err(err_msg)))
            }
        }

    }
}

impl Drop for NanomsgRequest {
    fn drop(&mut self) {
        if let Some(tx) = self.background_task_cancel_tx.take() {
            let _ = tx.send(());
        }
    }
}


async fn reply_fut(rx       : oneshot::Receiver<Vec<u8>>,
                   wait     : bool,
                   timeout  : Option<std::time::Duration>) -> io::Result<Vec<u8>> {
    let default_dur = std::time::Duration::from_millis(15000);
    if wait {
        tokio::time::delay_for(timeout.unwrap_or_else(|| std::time::Duration::from_millis(1000))).await;
        match futures::poll!(rx) {
            Poll::Ready(Ok(reply)) => {
                Ok(reply)
            },
            Poll::Ready(Err(err)) => {
                Err(other_err(format!("Got error on reply channel. {}", err)))
            }
            Poll::Pending => {
                Err(other_err("Timeout."))
            }
        }
    } else {
        let dur = timeout.unwrap_or(default_dur);
        match tokio::time::timeout(dur, rx).await {
            Ok(reply_res) => {
                match reply_res  {
                    Ok(reply) => {
                        Ok(reply)
                    }
                    Err(err) => {
                        Err(other_err(format!("Got error on reply channel. {}", err)))
                    }
                }
            },
            Err(_err) => {
                Err(other_err("Timeout."))
            }
        }
    }
}

fn spawn_background_task(address                 : String,
                         request_rx              : mpsc::UnboundedReceiver<Request>,
                         background_task_closer  : oneshot::Receiver<()>,
                         socket_options          : SocketOptions, 
                         bg_task_state           : BackgroundTaskStateAtomic) {
    let background_task = NanomsgRequestTask::new(address,
                                                  request_rx,
                                                  socket_options,
                                                  bg_task_state);
    tokio::spawn(async move {
        tokio::select!{
            reason = background_task.run() => {
                info!("Background task is dropped. Caused by inner error. {:?}", reason);
            },
            _ = background_task_closer => {
                info!("Background task is dropped by close signal!");
            }
        }
    });
}

struct NanomsgRequestTask {
    address             : String,
    socket_options      : SocketOptions,
    request_channel     : Option<mpsc::UnboundedReceiver<Request>>,
    state               : BackgroundTaskStateAtomic,
}

impl NanomsgRequestTask {
    fn new(address          : String,
           request_rx       : mpsc::UnboundedReceiver<Request>,
           socket_options   : SocketOptions,
           state            : BackgroundTaskStateAtomic) -> Self {
        Self {
            address,
            socket_options,
            state,
            request_channel     : Some(request_rx),
        }
    }

    async fn run(mut self) -> io::Result<()> {
        loop {

            let otf_requests = OnTheFlightRequests::new(); 
            
            let mut socket = match TcpStream::connect(&self.address).await {
                Ok(socket) => socket,
                Err(err) => {
                    if let Some(dur) = self.socket_options.get_reconnect_try_interval() {
                        self.state.set_reconnecting();
                        tokio::time::delay_for(dur).await;
                        continue
                    } else {
                        self.state.set_error(); 
                        return Err(err)
                    } 
                }
            };
            
            if let Err(err) = set_socket_options(&mut socket, &self.socket_options) {
                error!("Couldn't set socket options. {}", err);
            }

            match handshake(&mut socket).await {
                Ok(_) => {},
                Err(_err) => {
                    if let Some(dur) = self.socket_options.get_reconnect_try_interval() {
                        self.state.set_reconnecting(); 
                        tokio::time::delay_for(dur).await;
                        continue
                    } else {
                        self.state.set_error(); 
                        return Err(other_err("Handshake error")) 
                    } 
                }
            }

            self.state.set_connected(); 
            info!("Connected"); 
            let (read_half, write_half) = socket.split();

            let reader = Reader::new(read_half, otf_requests.clone());
            let mut writer = Writer::new(write_half,
                                         otf_requests.clone(),
                                         self.request_channel.take().unwrap());

            let res = tokio::select!{
                res = reader.run() => {
                    res
                },
                res = writer.run() => {
                    res
                }
            };

            if let Some(dur) = self.socket_options.get_reconnect_try_interval() {
                self.state.set_reconnecting(); 
                self.request_channel = Some(writer.request_channel);
                tokio::time::delay_for(dur).await;
                continue
            } else {
                self.state.set_error(); 
                return res
            }
        }
    }
}

fn set_socket_options(socket         : &mut TcpStream,
                      socket_options : &SocketOptions) -> io::Result<()>{

    let nodelay = socket_options.get_tcp_nodelay();
    socket.set_nodelay(nodelay)?;

    let keepalive = socket_options.get_tcp_keepalive();
    socket.set_keepalive(keepalive)?;

    let linger = socket_options.get_tcp_linger();
    socket.set_linger(linger)?;

    Ok(())
}

async fn handshake(socket: &mut TcpStream) -> io::Result<()> {
    socket.write_all(&REQ_HANDSHAKE_PACKET[..]).await?;
    let mut rep_handshake = [0; 8];
    socket.read_exact(&mut rep_handshake).await?;
    if rep_handshake != REP_HANDSHAKE_PACKET {
        Err(wrong_protocol_err())
    } else {
        Ok(())
    }
}

#[derive(Clone)]
struct OnTheFlightRequests {
    reply_channels: Arc<Mutex<TtlCache<u32, oneshot::Sender<Vec<u8>>>>>,
}

impl OnTheFlightRequests {
    fn new() -> Self {
        Self {
            reply_channels : Arc::new(Mutex::new(TtlCache::new(1024*64)))
        }
    }

    async fn add_reply_chan(&mut self,
                            reply_sender: oneshot::Sender<Vec<u8>>,
                            request_id: u32) {

        let dur = std::time::Duration::from_secs(1);
        self.reply_channels
            .lock()
            .await
            .insert(request_id, reply_sender, dur);
    }

    async fn reply_to(&mut self, request_id: u32, payload: Vec<u8>) {
        if let Some(sender) = self.reply_channels.lock().await.remove(&request_id) {
            if sender.send(payload).is_err() {
                error!("Couldn't send incoming reply to ReplyFuture.");
            }
        } else {
            info!("Request is most probably timedout");
            // TODO decide what we should do here 
        }
    }
}



struct Reader<'a>{
    socket          : ReadHalf<'a>,
    otf_requests    : OnTheFlightRequests,
}

impl <'a> Reader <'a> {
    fn new(socket: ReadHalf<'a>, otf_requests: OnTheFlightRequests) -> Self {
        Self {
            socket,
            otf_requests,
        }
    }

    pub async fn run(mut self) -> io::Result<()> {
        loop {
            let payload_size = self.socket.read_u64().await?;
            let request_id = self.socket.read_u32().await? & 0x7FFFFFFF;
            let mut payload = vec![0; payload_size as usize - 4];
            self.socket.read_exact(&mut payload).await?;
            self.otf_requests.reply_to(request_id, payload).await;
        }
    }
}


struct Request {
    payload         : Vec<u8>,
    reply_sender    : oneshot::Sender<Vec<u8>>,
}

struct Writer<'a> {
    socket          : WriteHalf<'a>,
    otf_requests    : OnTheFlightRequests,
    request_id      : u32,
    request_channel : mpsc::UnboundedReceiver<Request>,
}

impl <'a> Writer <'a> {
    fn new(socket           : WriteHalf<'a>,
           otf_requests     : OnTheFlightRequests,
           request_channel  : mpsc::UnboundedReceiver<Request>) -> Self {
        Self {
            socket,
            otf_requests,
            request_channel,
            request_id      : produce_request_id(),
        }
    }

    async fn send_request(&mut self, payload: &[u8]) -> io::Result<()> {
        self.advance_request_id();
        let size = payload.len() as u64 + 4;
        self.socket.write_u64(size).await?;
        self.socket.write_u32(self.request_id | 0x80000000).await?;
        self.socket.write_all(payload).await
    }

    fn advance_request_id(&mut self) {
        if self.request_id == 0x7FFFFFFF {
            self.request_id = 0;
        } else {
            self.request_id += 1;
        }
    }

    async fn run(&mut self) -> io::Result<()> {
        loop {
            if let Some(request) = self.request_channel.recv().await {

                if request.reply_sender.is_closed() { 
                    continue
                }

                self.send_request(&request.payload).await?;

                self.otf_requests.add_reply_chan(request.reply_sender, self.request_id).await;
            } else {
                info!("Request channel is closed.");
                // TODO return Error
                break;
            }
        }
        Ok(())
    }
}

// TODO request id must be produced randomly
fn produce_request_id() -> u32 {
    234234
}

fn other_err<E>(error_msg: E) -> std::io::Error 
    where E: Into<Box<dyn std::error::Error + Send + Sync>>{
    std::io::Error::new(ErrorKind::Other, error_msg)
}

fn wrong_protocol_err() -> std::io::Error {
    std::io::Error::new(ErrorKind::Other, "Wrong protocol.")
}

