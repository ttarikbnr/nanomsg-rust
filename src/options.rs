use std::time::Duration;

#[derive(Clone)]
pub struct SocketOptions {
    auto_reconnect              : bool,
    reconnect_try_interval      : Option<Duration>,
    max_reconnection_try        : Option<usize>,
    handshake_timeout           : Option<Duration>,
    tcp_nodelay                 : bool,
    tcp_keepalive               : Option<Duration>,
    tcp_linger                  : Option<Duration>,
}

impl Default for SocketOptions {
    fn default() -> Self {
        Self {
            auto_reconnect              : true,
            reconnect_try_interval      : Some(Duration::from_millis(10000)),
            max_reconnection_try        : None,
            handshake_timeout           : Some(Duration::from_millis(10000)),
            tcp_nodelay                 : false,
            tcp_keepalive               : None,
            tcp_linger                  : None,
        }
    }
}

impl SocketOptions {
    pub fn get_auto_reconnect(&self) -> bool {
        self.auto_reconnect
    }

    pub fn set_auto_reconnect(&mut self, auto_reconnect: bool) {
        self.auto_reconnect = auto_reconnect;
    }

    pub fn get_reconnect_try_interval(&self) -> Option<Duration> {
        self.reconnect_try_interval
    }

    pub fn set_reconnect_try_interval(&mut self,
                                      reconnect_try_interval: Option<Duration>) {
        self.reconnect_try_interval = reconnect_try_interval; 
    }

    pub fn get_max_reconnection_try(&self) ->Option<usize> {
        self.max_reconnection_try
    }

    pub fn set_max_reconnection_try(&mut self,
                                    max_reconnection_try: Option<usize>) {
        self.max_reconnection_try = max_reconnection_try;
    }

    pub fn get_handshake_timeout(&self) -> Option<Duration> {
        self.handshake_timeout
    }

    pub fn set_handshake_timeout(&mut self,
                                 handshake_timeout: Option<Duration>) {
        self.handshake_timeout = handshake_timeout;
    }

    pub fn get_tcp_nodelay(&self) -> bool {
        self.tcp_nodelay
    }

    pub fn set_tcp_nodelay(&mut self, tcp_nodelay : bool) {
        self.tcp_nodelay = tcp_nodelay;
    }

    pub fn get_tcp_keepalive(&self) -> Option<Duration> {
        self.tcp_keepalive
    }

    pub fn set_tcp_keepalive(&mut self, tcp_keepalive : Option<Duration>) {
        self.tcp_keepalive = tcp_keepalive;
    }

    pub fn get_tcp_linger(&self) -> Option<Duration> {
        self.tcp_linger
    }

    pub fn set_tcp_linger(&mut self, tcp_linger: Option<Duration>) {
        self.tcp_linger = tcp_linger;
    }
}