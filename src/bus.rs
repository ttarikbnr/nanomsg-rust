use async_stream::stream;
use std::{io::ErrorKind, pin::Pin, task::{Poll, Context}};
use futures::{Future, FutureExt, Stream};
use tokio::net::{TcpStream, tcp::WriteHalf, tcp::ReadHalf};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use crate::options::SocketOptions;
use tokio::sync::Mutex;
use std::sync::Arc;

const BUS_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x70, 0x00, 0x00];

enum State {
    Connecting,
    Connected
}

pub struct NanomsgBus {
    address     : String,
    stream      : Option<TcpStream>,
    // read_half   : Option<tokio::io::ReadHalf<TcpStream>>,
    // write_half  : Option<tokio::io::WriteHalf<TcpStream>>,
    options     : SocketOptions,
    state       : State
}

impl NanomsgBus {
    pub fn new( address: String )  -> Self {
        Self {
            address,
            stream: None,
            // read_half: None,
            // write_half: None,
            options: SocketOptions::default(),
            state: State::Connecting
        }
    }
    
    pub fn new_with_socket_options( address: String,
                options: SocketOptions)  -> Self {
        Self {
            address,
            stream: None,
            // read_half: None,
            // write_half: None,
            options,
            state: State::Connecting
        }
    }
    
    pub async fn connect(&mut self) -> std::io::Result<()>{
        let mut socket = TcpStream::connect(&self.address).await?; 
        socket.write_all(&BUS_HANDSHAKE_PACKET[..]).await?;
        let mut incoming_handshake= [0u8; 8];
        socket.read_exact(&mut incoming_handshake).await?;
        if incoming_handshake != BUS_HANDSHAKE_PACKET {
            return Err(other_err("Bus socket wrong handshake."))
        }
        // let (read_half, write_half) = tokio::io::split(socket);
        self.stream = Some(socket);
        // self.read_half = Some(read_half);
        // self.write_half = Some(write_half);
        Ok(())
    }

    pub async fn read(&mut self) -> std::io::Result<Vec<u8>> {
        loop {
            match self.state {
                State::Connecting => {
                    if let Err(err) = self.connect().await{
                        log::error!("Got error while connecting to bus {}", err);
                        if let Some(dur) = self.options.get_reconnect_try_interval() {
                            tokio::time::delay_for(dur).await;
                        }
                        continue
                    }
                    self.state = State::Connected;
                }
                State::Connected => {
                    match self.stream.as_mut() {
                        Some(socket) => {
                            match read(socket).await {
                                Ok(packet) => {
                                    return Ok(packet) 
                                }
                                Err(err) => {
                                    log::error!("Error in bus socket: {}", err);
                                    self.state = State::Connecting;
                                    continue
                                }
                            }
                        }
                        None => {
                            self.state = State::Connecting;
                            continue
                        }
                    }
                }
            }
        }
    }

    pub async fn write(&mut self, payload: Vec<u8>) -> std::io::Result<()> {
        loop{
            match self.state {
                State::Connecting => {
                    if let Some(dur) = self.options.get_reconnect_try_interval() {
                        tokio::time::delay_for(dur).await;
                    }
                    continue
                }
                State::Connected => {
                    match self.stream.as_mut() {
                        Some(socket) => {
                            write(socket, payload).await?;
                            return Ok(())
                        }
                        None => {
                            if let Some(dur) = self.options.get_reconnect_try_interval() {
                                tokio::time::delay_for(dur).await;
                            }
                            continue
                        }
                    }
                }
            }
        }
    }

    pub fn split<'a>(&'a mut self) -> Option<(NanomsgBusReader<'a>, NanomsgBusWriter<'a>)> {
        self.stream.as_mut().map(|socket| {
            let (readhalf, writehalf) = socket.split();
            (
                NanomsgBusReader{stream: readhalf},
                NanomsgBusWriter{stream: writehalf}
            )
        })
    }
}


pub struct NanomsgBusReader <'a> {
    stream: ReadHalf<'a>,
}

impl <'a> NanomsgBusReader <'a> {
    pub async fn read(&mut self) -> std::io::Result<Vec<u8>> {
        match read(&mut self.stream).await {
            Ok(packet) => {
                return Ok(packet) 
            }
            Err(err) => {
                let err_msg  = format!("Error in bus socket reader half : {}", err);
                return Err(other_err(err_msg))
            }
        }
    }
}

pub struct NanomsgBusWriter <'a> {
    stream: WriteHalf<'a>,
}

impl <'a> NanomsgBusWriter <'a> {
    pub async fn write(&mut self, packet: Vec<u8>) -> std::io::Result<()> {
        match write(&mut self.stream, packet).await {
            Ok(_) => {
                return Ok(()) 
            }
            Err(err) => {
                let err_msg  = format!("Error in bus socket writer half : {}", err);
                return Err(other_err(err_msg))
            }
        }
    }
}



pub struct NanomsgBusStream {
    bus : Arc<Mutex<NanomsgBus>>,
    
    read_poll_future: Option<Pin<Box<dyn Future<Output = std::io::Result<Vec<u8>>> + Send >>>,
    // write_poll_future: Option<Pin<Box<dyn Future<Output = std::io::Result<()>> + Send>>>,
}

impl NanomsgBusStream {
    pub fn new( address: String )  -> Self {
        let bus = Arc::new(Mutex::new(NanomsgBus::new(address)));

        Self {
            bus,
            read_poll_future: None,
            // write_poll_future: None,
        }
    }

    fn create_read_poll_future(&self) -> Pin<Box<dyn Future<Output = std::io::Result<Vec<u8>>> + Send >> {
        let bus = self.bus.clone();
        async move {
            let mut lock = bus.lock().await;
            lock.read().await
        }
        .boxed()
    }

    // fn create_write_poll_future(&self, payload: Vec<u8>) -> Pin<Box<dyn Future<Output = std::io::Result<()>> + Send>> {
    //     let bus = self.bus.clone();
    //     async move {
    //         let mut lock = bus.lock().await;
    //         lock.write(payload).await
    //     }
    //     .boxed()
    // }
}


impl Stream for NanomsgBusStream {
    type Item = Vec<u8>;

    fn poll_next(
        mut self: Pin<&mut Self>, 
        cx: &mut Context<'_>
    ) -> Poll<Option<Self::Item>> {
        loop {
            let poll_future = self.read_poll_future.take();
            if let Some(mut poll_future) = poll_future {
                match poll_future.as_mut().poll(cx) {
                    Poll::Ready(p) => {
                        self.read_poll_future = Some(self.create_read_poll_future());
        
                        return match p {
                            Ok(p) => Poll::Ready(Some(p)),
                            Err(_) => Poll::Ready(None),
                        }
                    }
                    Poll::Pending => {
                        self.read_poll_future = Some(poll_future);
                        return Poll::Pending
                    }
                }
            } else {
                self.read_poll_future = Some(self.create_read_poll_future());
            }
        }
    }
}

// impl Sink<Vec<u8>> for NanomsgBusSS {
//     type Error = std::io::Error;

//     fn poll_ready(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         let poll_future = self.write_poll_future.take();
//         if let Some(mut poll_future) = poll_future {
//             match poll_future.as_mut().poll(cx) {
//                 Poll::Ready(p) => {       
//                     return match p {
//                         Ok(_) => Poll::Ready(Ok(())),
//                         err @ Err(_) => Poll::Ready(err),
//                     }
//                 }
//                 Poll::Pending => {
//                     self.write_poll_future = Some(poll_future);
//                     return Poll::Pending
//                 }
//             }
//         } else {
//             Poll::Ready(Ok(()))
//         }
//     }

//     fn start_send(mut self: Pin<&mut Self>, item: Vec<u8>) -> Result<(), Self::Error> {
//         self.write_poll_future = Some(self.create_write_poll_future(item));
//         Ok(())
//     }

//     fn poll_flush(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         loop {
//             let poll_future = self.write_poll_future.take();
//             if let Some(mut poll_future) = poll_future {
//                 match poll_future.as_mut().poll(cx) {
//                     Poll::Ready(p) => {
        
//                         return match p {
//                             Ok(_) => Poll::Ready(Ok(())),
//                             err @ Err(_) => Poll::Ready(err),
//                         }
//                     }
//                     Poll::Pending => {
//                         return Poll::Pending
//                     }
//                 }
//             } else {
//                 return Poll::Ready(Ok(()))
//             }
//         }
//     }

//     fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
//         Poll::Ready(Ok(()))
//     }
// }


async fn read<T>(reader: &mut T) -> std::io::Result<Vec<u8>>
    where T : AsyncReadExt + Unpin {

    let payload_size = reader.read_u64().await?;
    let mut buf = vec![0u8; payload_size as usize];
    reader.read_exact(&mut buf).await?;
    Ok(buf)
}

async fn write<T>(writer: &mut T, payload: Vec<u8>) -> std::io::Result<()> 
    where T : AsyncWriteExt + Unpin{
    let payload_size : u64 = payload.len() as _;
    writer.write_u64(payload_size).await?;
    writer.write_all(&payload).await?;
    Ok(())
}

fn other_err<E>(error_msg: E) -> std::io::Error 
    where E: Into<Box<dyn std::error::Error + Send + Sync>>{
    std::io::Error::new(ErrorKind::Other, error_msg)
}


pub fn nanomsg_bus_stream(address: String) -> Pin<Box<dyn Stream<Item=Vec<u8>> + Send + 'static >>{
    let mut socket = NanomsgBus::new(address);
    let stream = stream! {
        loop {
            match socket.read().await {
                Ok(packet) => {
                    yield packet
                },
                Err(err) => {
                    log::error!("Got error in nanomsg bus socket. {}", err);
                }
            }
        }
    };

    Box::pin(stream)
}