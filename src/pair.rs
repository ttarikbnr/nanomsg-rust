use std::io::{self, ErrorKind};
use tokio::net::{TcpStream, TcpListener};
use tokio::net::tcp::{WriteHalf, ReadHalf};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use crate::options::SocketOptions;

const PAIR_HANDSHAKE_PACKET : [u8; 8] = [0x00, 0x53, 0x50, 0x00, 0x00, 0x10, 0x00, 0x00];


pub struct NanomsgPairListener {
    binding_port        : Option<u16>,
    listener            : Option<TcpListener>,
    socket_options      : SocketOptions,
}

impl NanomsgPairListener {
    pub fn new() -> Self {
        let mut socket_options = SocketOptions::default();
        socket_options.set_auto_reconnect(false);

        Self {
            binding_port    : None,
            listener        : None,
            socket_options,
        }
    }

    pub fn set_options(&mut self, socket_options: SocketOptions) {
        self.socket_options = socket_options;
    }

    pub async fn bind(&mut self, binding_port: u16) -> io::Result<()> {
        let listener        = TcpListener::bind(format!("0.0.0.0:{}", binding_port)).await?;
        self.binding_port   = Some(binding_port);
        self.listener       = Some(listener);
        Ok(())
    }

    // TODO add to handshake a timeout
    pub async fn accept(&mut self) -> io::Result<NanomsgPair> {
        if let Some(ref mut listener) = self.listener {
            let (mut socket, address) = listener.accept().await?; 

            handshake(&mut socket).await?;

            let address = format!("{}", address);
            let socket_options = self.socket_options.clone();
            let pair_socket = NanomsgPair::new_from_socket(socket, address, socket_options);
            Ok(pair_socket)
        } else {
            panic!("Listener is not bound to any port!"); 
        }
    }
}

// TODO use socket options
pub struct NanomsgPair {
    address             : Option<String>,
    socket              : Option<TcpStream>,
    socket_options      : SocketOptions,
}

impl NanomsgPair {
    pub fn new() -> Self {
        Self {
            address         : None,
            socket          : None,
            socket_options  : SocketOptions::default(),
        }
    }

    pub fn set_socket_options(&mut self, socket_options: SocketOptions) {
        self.socket_options = socket_options;
    }

    fn new_from_socket(socket           : TcpStream,
                       address          : String,
                       socket_options   : SocketOptions) -> Self {
        Self {
            socket          : Some(socket),
            address         : Some(address),
            socket_options,
        }
    }

    pub async fn connect(&mut self, address: String) -> io::Result<()> {
        if self.socket.is_some() {
            return Ok(());
        }

        let mut socket = TcpStream::connect(&address).await?;

        handshake(&mut socket).await?;

        socket.set_linger(self.socket_options.get_tcp_linger())?;
        socket.set_keepalive(self.socket_options.get_tcp_keepalive())?;
        socket.set_nodelay(self.socket_options.get_tcp_nodelay())?;

        self.address = Some(address);
        self.socket  = Some(socket);
        Ok(())
    }

    pub async fn read(&mut self) -> io::Result<Vec<u8>> {
        if let Some(ref mut socket) = self.socket {
            read(socket).await
        } else {
            panic!("Socket is none.");
        }
    }

    pub async fn write(&mut self, payload: Vec<u8>) -> io::Result<()> {
        if let Some(ref mut socket) = self.socket {
            write(socket, payload).await
        } else {
            panic!("Socket is none.");
        }
    }

    pub fn split(&mut  self) -> (NanomsgPairReader<'_>, NanomsgPairWriter<'_>) {
        if let Some(ref mut socket) = self.socket {
            let (readhalf, writehalf) = socket.split();
            let reader = NanomsgPairReader::new(readhalf);
            let writer = NanomsgPairWriter::new(writehalf);
            (reader, writer)
        } else {
            panic!("Socket is none.");
        }
    }
}

pub struct NanomsgPairReader<'a> {
    socket: ReadHalf<'a>,
}

impl <'a> NanomsgPairReader <'a> {
    pub fn new(socket: ReadHalf<'a>) -> Self {
        Self {
            socket
        }
    }

    pub async fn read(&mut self) -> io::Result<Vec<u8>> {
        read(&mut self.socket).await
    }
}

pub struct NanomsgPairWriter<'a> {
    socket: WriteHalf<'a>,
}

impl <'a> NanomsgPairWriter <'a> {
    pub fn new(socket: WriteHalf<'a>) -> Self {
        Self {
            socket
        }
    }

    pub async fn write(&mut self, payload: Vec<u8>) -> io::Result<()> {
        write(&mut self.socket, payload).await
    }
}

async fn handshake(socket: &mut TcpStream) -> io::Result<()> {
    socket.write_all(&PAIR_HANDSHAKE_PACKET[..]).await?;
    let mut pair_handshake = [0; 8];
    socket.read_exact(&mut pair_handshake).await?;
    if pair_handshake != PAIR_HANDSHAKE_PACKET {
        Err(wrong_protocol_err())
    } else {
        Ok(())
    }
}

async fn read<T>(reader: &mut T) -> std::io::Result<Vec<u8>>
    where T : AsyncReadExt + Unpin{
    let payload_size = reader.read_u64().await?;
    let mut buf = vec![0u8; payload_size as usize];
    reader.read_exact(&mut buf).await?;
    Ok(buf)
}

async fn write<T>(writer: &mut T, payload: Vec<u8>) -> std::io::Result<()>
    where T: AsyncWriteExt + Unpin{
    let payload_size = payload.len() as u64;
    writer.write_u64(payload_size).await?;
    writer.write_all(&payload).await?;
    Ok(())
}

fn wrong_protocol_err() -> std::io::Error {
    std::io::Error::new(ErrorKind::Other, "Wrong protocol.")
}
