use tokio;
use anyhow::Result;
use nanomsg_rust::NanomsgRequest;

#[tokio::main]
async fn main() -> Result<()> {
    let socket = NanomsgRequest::new("127.0.0.1:5659".to_string());
    async {
        loop {
            let res = socket.request(b"baba".to_vec()).await;
            if res.is_err() {
                println!("error");
            }
            if let Ok(reply) = res {
                println!("Reply {}", String::from_utf8_lossy(reply.as_ref()));
            }
        }
    }.await;
    
    Ok(())
}
